from sqlalchemy import Column, Integer, String, ForeignKey, Table
from sqlalchemy.ext.associationproxy import association_proxy
from sqlalchemy.orm import declarative_base, relationship

Base = declarative_base()


class Actor(Base):
    __tablename__ = "actor"
    id = Column(Integer, primary_key=True)
    tmdb_id = Column(Integer, unique=True)
    name = Column(String(64), unique=True, index=True)
    mv = relationship("Movie", secondary=lambda: actormovies_table)

    def __init__(self, name):
        self.name = name

    # proxy the 'movie' attribute from the 'mv' relationship
    movies = association_proxy("mv", "movie")


class Movie(Base):
    __tablename__ = "movie"
    id = Column(Integer, primary_key=True)
    tmdb_id = Column(Integer, unique=True)
    movie = Column("movie", String(64))

    def __init__(self, movie):
        self.movie = movie


actormovies_table = Table(
    "actormovies",
    Base.metadata,
    Column("actor_id", Integer, ForeignKey("actor.id"), primary_key=True),
    Column("movie_id", Integer, ForeignKey("movie.id"), primary_key=True),
)
