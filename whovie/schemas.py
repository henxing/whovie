from typing import List, Optional

from pydantic import BaseModel


class ActorBase(BaseModel):
    name: str
    tmdb_id: int
    movies: List[str]


class ActorCreate(ActorBase):
    pass


class Actor(ActorBase):
    id: int

    class Config:
        orm_mode = True


class MovieBase(BaseModel):
    movie: str
    tmdb_id: int


class MovieCreate(MovieBase):
    pass


class Movie(MovieBase):
    id: int

    class Config:
        orm_mode = True
