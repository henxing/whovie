from fastapi import Depends, FastAPI, HTTPException
from fastapi.responses import FileResponse, JSONResponse
from db import get_db, engine
from sqlalchemy.orm import Session
import uvicorn
from typing import List, Optional
from fastapi.encoders import jsonable_encoder

from whovie import models
from whovie import schemas
from whovie import __version__


app = FastAPI(
    title="Whovie backend",
    description="Serves requests for actor/movie teamups",
    version=__version__,
)
favicon_path = "video_camera.ico"
models.Base.metadata.create_all(bind=engine)


@app.exception_handler(Exception)
def validation_exception_handler(request, err):
    base_error_message = f"Failed to execute: {request.method}: {request.url}"
    return JSONResponse(
        status_code=400, content={"message": f"{base_error_message}. Detail: {err}"}
    )


@app.get("/favicon.ico", include_in_schema=False)
def favicon():
    return FileResponse(favicon_path)


if __name__ == "__main__":
    uvicorn.run("main:app", port=9000, reload=True)
