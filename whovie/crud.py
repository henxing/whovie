import logging
import os
from typing import List, Optional, Set

import tmdbsimple as tmdb


tmdb.API_KEY = os.getenv("TMDB_API_KEY")
tmdb.REQUESTS_TIMEOUT = 5
search = tmdb.Search()


def _get_cast_set(person_name: str, include_adult: bool) -> Set[str]:
    """
    Returns the set of movie titles in which the given person appeared as a part of the cast.

    :param person_name: the name of the cast member
    :type person_name: str
    :param include_adult: If true, include listings from adult actors/movies
    :type include_adult: bool
    :rtype: Set[str]
    """
    try:
        response = search.person(query=person_name, include_adult=include_adult)
        results = response.get("results", [])
        person = tmdb.People(id=results[0]["id"])
    except:
        logging.error(f"Actor '{person_name}' not found", exc_info=True)
        cast_set = set()
    else:
        cast_set = set(movie["title"] for movie in person.movie_credits().get("cast", []))

    return cast_set


def read_names(name_list: List[str], include_adult: Optional[bool] = False) -> Set[str]:
    """
    Takes a list of names and finds the intersection of all movies in which they appeard together.

    :param name_list: the list of names
    :type name_list: List[str]
    :param include_adult: If true, include listings from adult actors/movies
    :type include_adult: bool
    :rtype: Set[str]
    """
    movie_sets = [_get_cast_set(name, include_adult) for name in name_list]
    return list(movie_sets[0].intersection(*movie_sets[1:]))


if __name__ == "__main__":
    import argparse

    parser = argparse.ArgumentParser(
        description="Find the set of movies containing all of the given actors."
    )
    parser.add_argument(
        "name_list", type=str, nargs="+", help="The list of actors' names to search"
    )
    parser.add_argument("--include-adult", action="store_true")
    args = parser.parse_args()
    movies = read_names(**vars(args))

    print(movies)
