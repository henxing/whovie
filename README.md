# whovie

Provides interesting insights into movies and actors.

# Development

See the [contribution guide](CONTRIBUTING.md) for how to contribute to this project.

# Running the backend

```
whovie$ poetry shell
(.venv) whovie$ python main.py
```
