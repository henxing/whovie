# Contributing

## Set up development environment

1. Clone the repository
    ```
    ~$ git clone git@gitlab.com:henxing/whovie.git
    ```
1. Install dependencies with [poetry](https://python-poetry.org/):
    ```
    ~$ cd whovie
    whovie$ poetry install
    ```
1. Install [pre-commit](https://pre-commit.com/) hooks:
    ```
    whovie$ poetry shell
    (.venv) whovie$ pre-commit install
    (.venv) whovie$ pre-commit run --all-files # Optionally
    ```
1. Obtain an [API key](https://www.themoviedb.org/documentation/api)
1. Set your API key environment variable:
    ```
    (.venv) whovie$ export TMDB_API_KEY=9e45fb707e1111ec99107b068897f2fe
    ```
